# Website for sunshine europe project

Deployed on [eurosfordocs.eu](eurosfordocs.eu).

## Usage

In this project everything is execupted within a docker, so there is nothing to install on the server itself. To run it, simply set the variable in the .env file and call: 

```bash
docker-compose up 
```

## Loading the data

If you want to load or reload data from the csv files, call: 
```bash
docker-compose up dataload
```
In details, this will: 
* download data from the S3 fodler. If some new data is downloaded: 
    * drop all tables in the postgres DB
    * recreate the DB from the schema
    * insert the content of the CSV files in the table
    * create the table "enhanced_link" from the query `create_views.sql`
    * run any sql files in the folder `docker/postgres/generate_csv` and save their result as csv files in /public/downlads. These CSVs will then be available at `eurosfordocs.eu/downloads/xxxx.csv`

On the server this is automated and runs every night

If you want to load the local data, without checking the S3 data, please call: 

```bash
docker-compose run -e force_reset_from_local_data=True data_load
```

## Admin scripts
can be used to back-up and restore the Metabase config. 
