#!/usr/bin/env bash
# Backup metabase data from host to local file
set -euo pipefail

host=${1:-"localhost"}
path=${2:-"/backup/"}

echo ${host}

BACKUP_FILE=${path}pg_dump_${host}_`date +%Y-%m-%d_%H-%M-%S`.sql
echo Dump metabase backend on ${host} to ${BACKUP_FILE}
echo You will be prompted to give the password for Postgres metabase backend

docker-compose exec mb-postgres pg_dump -h ${host} -p 5432 -U postgres > ${BACKUP_FILE}

echo Done.

echo "You can restore this metabase data locally, with command 'admin_scripts/restore_mb_postgres.sh ${BACKUP_FILE}'"
