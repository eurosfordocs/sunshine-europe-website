version: "3"
services:
  nginx:
    image: nginx:latest
    depends_on:
      - metabase
    volumes:
    - ./docker/nginx/nginx.conf:/etc/nginx/nginx.conf:ro
    - ./docker/wp/html:/var/www/html
    - ./docker/nginx/nginx-logs:/logs:rw
    - ./public:/public:ro
    ports:
    - 80:80
    links:
    - wordpress

  wp_mariadb:
    image: mariadb
    volumes:
      - ./docker/wp/data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: ${WP_DB_ROOT_PWD}
      MYSQL_DATABASE: wp_db
      MYSQL_USER: ${WP_USER_NAME}
      MYSQL_PASSWORD: ${WP_USER_PSW}

  wordpress:
    image: wordpress:php7.4-fpm-alpine
    volumes:
      - ./docker/wp/html:/var/www/html
    depends_on:
      - wp_mariadb
    environment:
      WORDPRESS_DB_HOST: wp_mariadb
      MYSQL_ROOT_PASSWORD: ${WP_DB_ROOT_PWD}
      WORDPRESS_DB_NAME: wp_db
      WORDPRESS_DB_USER: ${WP_USER_NAME}
      WORDPRESS_DB_PASSWORD: ${WP_USER_PSW}
      WORDPRESS_TABLE_PREFIX: wp_
    links:
      - wp_mariadb

  postgres:
    image: postgres:11-alpine
    shm_size: 1g
    container_name: ${POSTGRES_CONTAINER_NAME}
    ports:
    - 5432:5432
    volumes:
      - ${NORMALIZED_DATA_FOLDER}:/home/data
      - ./docker/postgres:/home/scripts
    environment:
    - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}

  metabase:
    image: metabase/metabase:latest
    depends_on:
    - postgres
    - mb-postgres
    ports:
    - 3000:3000
    environment:
    - MB_DB_TYPE=postgres
    - MB_DB_DBNAME=postgres
    - MB_DB_PORT=5432
    - MB_DB_USER=postgres
    - MB_DB_PASS=${MB_POSTGRES_PASSWORD}
    - MB_DB_HOST=mb-postgres

  mb-postgres:
    image: postgres:11-alpine
    container_name: ${MB_POSTGRES_CONTAINER_NAME}
    volumes:
    - ./docker/mb-postgres/data:/var/lib/postgresql/data:rw
    environment:
    - POSTGRES_PASSWORD=${MB_POSTGRES_PASSWORD}
    - PGDATA=/var/lib/postgresql/data/pgdata
    ports:
    - 6543:5432

  schemacrawler:
    image: schemacrawler/schemacrawler:v15.05.01
    volumes:
    - ./schemas/normalized:/share
    - ./docker/schemacrawler/schemacrawler_entrypoint.sh:/home/schcrwlr/schemacrawler_entrypoint.sh
    environment:
    - POSTGRES_PASSWORD
    command: /bin/bash ./schemacrawler_entrypoint.sh

  data_load:
    image: python:3
    depends_on:
      - postgres
      - mb-postgres
    volumes:
      - ${NORMALIZED_DATA_FOLDER}:/home/data
      - /var/run/docker.sock:/var/run/docker.sock
      - .:/home
    command: sh -c "pip install -r /home/requirements.txt
              && cd home
              && python3 main.py init"
