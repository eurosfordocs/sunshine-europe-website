#!/usr/bin/env python3

import logging
import os
from src.postgresql import init_postgresql
from src.data_storage import download_normalized_data


if __name__ == '__main__':
    if os.environ.get('force_reset_from_local_data') == 'True':
        logging.info('Forcing the data reset from local data files')
        init_postgresql(reset=True)
    else:
        #download_normalized_data downloads any changed or new data, and return true if there were any.
        changes = download_normalized_data()
        if changes:
            logging.info('Changes in the S3 repo, we upgrade the data')
        # if there were changes, we re reimport the data in the DB.
            init_postgresql(reset=True)
        #init_postgresql(reset)
        else:
            logging.info('No changes in the S3 repo.')
