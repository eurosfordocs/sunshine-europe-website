#!/bin/bash
set -e

dir="/home/data"

for table in directory entity publication activity link entity_relation
do
    echo "Load ${table}"
    for csv in `find ${dir} \( -name "${table}__*" -o -name "${table}.csv" \)`
    do
        echo "- ${csv}"
        psql postgres --username postgres  -c "COPY ${table} from '${csv}' WITH (FORMAT CSV, HEADER, DELIMITER ',', QUOTE '\"', NULL '');"
    done
done

