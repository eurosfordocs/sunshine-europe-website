create table enhanced_link as (


with deduplication as (select * from entity_relation where relation_type = 'a_deduplicates_b')

select

    l.*,

    --publication fields
    publication.covered_area as publication_country,
    publication.source_url as publication_url,

    --source fields
    coalesce(clean_source.entity_id, l.source_organization_id) as clean_source_organization_id,
    coalesce(clean_source.full_name, source.full_name) as source_organisation_full_name,

    --recipient fields
    coalesce(clean_rec.entity_id, rec.entity_id) as clean_recipient_entity_id,
    coalesce(clean_rec.entity_type, rec.entity_type) as clean_recipient_entity_type,
    coalesce(clean_rec.full_name, rec.full_name) as recipient_entity_full_name,
    coalesce(clean_rec.city, rec.city) as recipient_entity_city,
    coalesce(clean_rec.is_person, rec.is_person) as recipient_entity_is_person,
    coalesce(clean_rec.person_specialty, rec.person_specialty) as recipient_person_specialty,

    coalesce(clean_rec.directory_id, rec.directory_id) as recipient_directory_id,
    coalesce(clean_rec.directory_entity_id, rec.directory_entity_id) as recipient_directory_entity_id,


    --activity fields
    a.name as related_activity_name

from link l
    left join entity source on l.source_organization_id = source.entity_id
    left join deduplication dedup_source on source.entity_id = dedup_source.entity_b
    left join entity clean_source on dedup_source.entity_a = clean_source.entity_id
    left join entity rec on l.recipient_entity_id = rec.entity_id
    left join deduplication dedup_rec on rec.entity_id = dedup_rec.entity_b
    left join entity clean_rec on dedup_rec.entity_a = clean_rec.entity_id
    left join publication using (publication_id)
    left join activity a on l.related_activity_id = a.activity_id
);


alter table enhanced_link ADD PRIMARY KEY (link_id);
alter table enhanced_link add constraint fk_el_clean_recipient  FOREIGN KEY (clean_recipient_entity_id) REFERENCES entity (entity_id);
alter table enhanced_link add constraint fk_el_recipient        FOREIGN KEY (recipient_entity_id)       REFERENCES entity (entity_id);

alter table enhanced_link add constraint fk_el_clean_source  FOREIGN KEY (clean_source_organization_id) REFERENCES entity (entity_id);
alter table enhanced_link add constraint fk_el_source        FOREIGN KEY (source_organization_id)       REFERENCES entity (entity_id);

alter table enhanced_link add constraint fk_el_publication  FOREIGN KEY (publication_id) REFERENCES publication (publication_id);


create table anonymous_link as (
select
--link fields
link_id,
type,
category,
details,
date,
year,
value_total_amount,
value_total_amount_eur,
currency,
is_aggregated,
number_of_aggregated_recipients,

--activity fields
related_activity_name,

--publication fields
publication_id,
publication_country,
publication_url,

-- source fields
source_organization_id,
clean_source_organization_id,
source_organisation_full_name,

-- recipient fields
recipient_entity_type,
recipient_entity_id,
recipient_entity_is_person,

case when recipient_entity_type in ('HCO', 'research and development') then recipient_entity_full_name else NULL end recipient_entity_full_name,
case when recipient_entity_type in ('HCO', 'research and development') then recipient_entity_city else NULL end recipient_entity_city

from enhanced_link
)
