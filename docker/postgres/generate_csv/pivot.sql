with source_ranking as (
select
clean_source_organization_id,
avg(rank) avg_rank,
avg(value_per_year) avg_publication_value,
count(distinct country) countries
from (
    select
    publication_country country,
    clean_source_organization_id,
    rank() over (partition by publication_country order by sum(value_total_amount_eur) / count(distinct year) desc),
    count(*) links,
    count(distinct year) years,
    sum(value_total_amount_eur) value_eur,
    sum(value_total_amount_eur) / count(distinct year) value_per_year
    from enhanced_link l
    where clean_source_organization_id != ''
    group by 1,2
) a
group by 1)

select
l.publication_country country,
l.year,
l.clean_source_organization_id company,
avg_rank,
countries company_present_in_countries,
sum(case when l.category = 'research and development' then l.value_total_amount_eur else 0 end) rnd,
sum(case when l.recipient_entity_type = 'HCO' and recipient_entity_id is not null then l.value_total_amount_eur else 0 end) hco_ind,
sum(case when l.recipient_entity_type = 'HCO' and recipient_entity_id is     null then l.value_total_amount_eur else 0 end) hco_agg,
sum(case when l.recipient_entity_type = 'HCP' and recipient_entity_id is not null then l.value_total_amount_eur else 0 end) hcp_ind,
sum(case when l.recipient_entity_type = 'HCP' and recipient_entity_id is     null then l.value_total_amount_eur else 0 end) hcp_agg,
sum(value_total_amount_eur) total,
count(*) links
from  enhanced_link l
left join source_ranking using (clean_source_organization_id)
where year > 2016 and avg_rank < 30 and countries > 4
group by 1,2,3,4,5


