#!/usr/bin/env bash
echo 'Running schema crawler against PostgreSQL'

/opt/schemacrawler/schemacrawler.sh -command=schema \
-infolevel=standard \
-portablenames=true \
-server=postgresql -host=postgres -port=5432 -user=postgres -password=${POSTGRES_PASSWORD} -database=postgres  \
-title 'Normalized sunshine schema' \
-only-matching \
-outputformat=pdf -outputfile=/share/relational_schema.pdf

echo 'Done. Relational diagram has been written in data/byproducts/relational_diagram'
