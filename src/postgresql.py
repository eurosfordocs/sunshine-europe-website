import logging
from time import sleep
import docker
import os
import csv

from sqlalchemy import create_engine, text
from sqlalchemy.engine import Engine
from sqlalchemy.exc import OperationalError
import sqlalchemy
from tableschema_sql import Storage

from src.params import POSTGRES_PASSWORD, POSTGRES_CONTAINER_NAME, MB_POSTGRES_PASSWORD, MB_POSTGRES_CONTAINER_NAME
from src.utils import get_all_schema, is_running_in_docker

POSTGRES_LOAD_TABLE_COMMAND = "bash /home/scripts/load_data.sh"
GENERATE_CSV_FOLDER = os.path.join("docker", "postgres", "generate_csv")
DOWNLOADS_FOLDER= os.path.join("public", "downloads")

POSTGRES_CREATE_VIEWS_COMMAND = f"psql postgres --username postgres  --single-transaction -f /home/scripts/create_views.sql"


def get_docker_container():
    docker_client = docker.from_env()
    for container in docker_client.containers.list(all=True):
        if container.name == POSTGRES_CONTAINER_NAME:
            return container
    raise EnvironmentError("the postgres docker doesn't exist! Run it once manually.")


def execute_and_write_csv(sql, out_fn, request_name):
    try:
        engine = get_postgres_engine()
        wait_for_postgres(engine)
        r = engine.execute(text(sql))
        rows = r.fetchall()
        if len(rows) > 0:
            with open(os.path.join(DOWNLOADS_FOLDER, out_fn), 'w') as out_f:
                myFile = csv.writer(out_f)
                myFile.writerow(r.keys())
                myFile.writerows(rows)
            logging.info(f"Executed {request_name}: {len(rows)} written to {out_fn}")
        else:
            logging.info(f"Executed {request_name}, but no result.")
    except sqlalchemy.exc.ProgrammingError as e:
        logging.info(f"SQL error in {request_name}: {str(e)}")


def generate_anonymous_export(where_clause, out_fn, request_name):
    sql = f"Select * from anonymous_link {where_clause}"
    execute_and_write_csv(sql, out_fn, request_name)


def generate_csvs():
    #clear the contents of the download folder
    for f in os.listdir(DOWNLOADS_FOLDER):
        if f != ".gitkeep":
            os.remove(os.path.join(DOWNLOADS_FOLDER, f))

    # run each of the queries in the generate_csv folder and saves their result as csv

    sql_files = [x for x in os.listdir(GENERATE_CSV_FOLDER) if x.endswith(".sql")]
    for sql_file in sql_files:
        with open(os.path.join(GENERATE_CSV_FOLDER, sql_file), 'r') as f:
            execute_and_write_csv(sql= f.read(), out_fn= f"{sql_file[0:-4]}.csv", request_name=sql_file)


    # generate anonymous exports
    # global_export
    generate_anonymous_export(where_clause="where year >= 2017",
                              out_fn="anonymous_link.csv",
                              request_name="anonymous_link_global_export")

    for c in ["Germany", "Switzerland", "Sweden", "Italy", "Spain", "UK", "Ireland", "Romania", "Portugal", "Denmark", "Belgium", "Greece"]:
        generate_anonymous_export(where_clause=f"where year >= 2017 and publication_country ='{c}' ",
                                  out_fn=f"anonymous_link_{c}.csv",
                                  request_name=f"anonymous_link_{c}")
        for y in [2017, 2018, 2019]:
            generate_anonymous_export(where_clause=f"where year = {y} and publication_country ='{c}'",
                                      out_fn=f"anonymous_link_{c}_{y}.csv",
                                      request_name=f"anonymous_link_{c}_{y}")



def init_postgresql(reset=True, should_generate_csvs=True):
    #start postgres container
    get_docker_container().restart()
    if reset:
        reset_postgresql_tables()
    load_postgresql_tables()
    create_views()
    delete_metabase_cache()
    if should_generate_csvs:
        generate_csvs()


def create_views():
    logging.info("Postgres: Create Materialized views")
    result = get_docker_container().exec_run(POSTGRES_CREATE_VIEWS_COMMAND)

    if result.exit_code != 0:
        logging.error(str(result.output))
        raise Exception(result.output)
    else:
        logging.info(str(result.output))


def load_postgresql_tables():
    logging.info("Load PostgreSQL tables with csv")
    result = get_docker_container().exec_run(POSTGRES_LOAD_TABLE_COMMAND)

    if result.exit_code != 0:
        logging.error(str(result.output).replace("\\n", "\n"))
        raise Exception(result.output)
    else:
        logging.info(str(result.output).replace("\\n", "\n"))


def reset_postgresql_tables():
    engine = get_postgres_engine()
    wait_for_postgres(engine)
    drop_all_tables_postgres(engine)
    create_postgresql_tables(engine)


def create_postgresql_tables(engine: Engine) -> None:
    logging.info("Create tables from schema in PostgreSQL")
    schemas = get_all_schema()
    storage = Storage(engine=engine)
    storage.create([schema.descriptor['name'] for schema in schemas],
                   [schema.descriptor for schema in schemas],
                   force=True)


def drop_all_tables_postgres(engine):
    """ This allow a much faster table creation. """
    logging.info("Drop all tables in PostgreSQL")
    engine.execute("""
    DROP SCHEMA public CASCADE;
    CREATE SCHEMA public;
    GRANT ALL ON SCHEMA public TO postgres;
    GRANT ALL ON SCHEMA public TO public;""")


def get_postgres_engine() -> Engine:
    postgres_host = 'postgres'
    return create_engine(f'postgresql://postgres:{POSTGRES_PASSWORD}@{postgres_host}:5432/postgres')


def wait_for_postgres(engine: Engine, max_waiting_time: int = 50):
    logging.info('Waiting until PostgreSQL accept connexions')
    for i in range(max_waiting_time):
        if does_postgres_accept_connection(engine):
            logging.info('PostgreSQL is ready to accept connexions')
            return
        logging.info('PostgreSQL is not ready to accept connexions, waiting {} more seconds'
                     .format(max_waiting_time - i))
        sleep(1)
    engine.connect()  # Raise exception


def does_postgres_accept_connection(engine: Engine) -> bool:
    """ Test if the target PostgreSQL database accept connexions
    """
    try:
        engine.connect()
    except OperationalError:
        return False
    else:
        return True


def delete_metabase_cache():
    logging.info("Delete Metabase cache")
    engine = get_mb_postgres_engine()
    wait_for_postgres(engine)
    query = text("TRUNCATE TABLE public.query_cache")
    engine.execute(query)
    logging.info("Deleted Metabase cache (truncate query_cache table)")


def get_mb_postgres_engine() -> Engine:
    return create_engine(f'postgresql://postgres:{MB_POSTGRES_PASSWORD}@{MB_POSTGRES_CONTAINER_NAME}:5432/postgres')


if __name__ == '__main__':
    init_postgresql()
