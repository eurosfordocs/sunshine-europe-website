import logging

from src.params import LOGGING_LEVEL

logging.basicConfig(level=LOGGING_LEVEL,
                    format='%(asctime)s :: %(levelname)s :: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    )
