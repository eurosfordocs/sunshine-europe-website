import logging
import subprocess

from src.postgresql import get_postgres_engine, does_postgres_accept_connection
from src.utils import is_running_in_docker

RUN_SCHEMACRAWLER_CONTAINER = 'docker-compose up schemacrawler'


def generate_relational_diagram():
    if is_running_in_docker():
        logging.info("Generate PostgreSQL tables within Docker")
        generate_postgresql_tables_within_docker()
    else:
        logging.info("Generate relational diagram from host")
        generate_relational_diagram_from_host()


def generate_postgresql_tables_within_docker():
    engine = get_postgres_engine()
    if not does_postgres_accept_connection(engine):
        logging.warning("PostgreSQL container is not started.")
        logging.warning(f"You should start PosgreSQL with : `{START_POSTGRES_CONTAINER_IN_BACKGROUND}`")
    logging.info(f"To create a relational diagram : `{RUN_SCHEMACRAWLER_CONTAINER}`")


def generate_relational_diagram_from_host():
    logging.info('Launch schemacrawler with docker-compose, to create a relational diagram')
    subprocess.run(RUN_SCHEMACRAWLER_CONTAINER.split())
