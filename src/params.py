import logging
import os

from dotenv import load_dotenv

load_dotenv()

POSTGRES_CONTAINER_NAME = os.getenv("POSTGRES_CONTAINER_NAME")
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', '')
LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'INFO')


MB_POSTGRES_USER = "postgres"
MB_POSTGRES_PASSWORD = os.getenv('MB_POSTGRES_PASSWORD', '')
MB_POSTGRES_CONTAINER_NAME = os.getenv("MB_POSTGRES_CONTAINER_NAME", '')


if LOGGING_LEVEL not in logging._nameToLevel:
    raise ValueError("LOGGING_LEVEL='{}' bug it should be in list [{}]"
                     .format(LOGGING_LEVEL, ','.join(logging._nameToLevel.keys())))
LOGGING_LEVEL = logging._nameToLevel[LOGGING_LEVEL]
