
import logging
import os
import pathlib
from dotenv import load_dotenv
logging.getLogger().setLevel(logging.INFO)
import subprocess
load_dotenv()

CONTAINER = os.getenv("CONTAINER")
DATA_FOLDER = "/home/data"


def init():
    os.environ["OS_AUTH_URL"] = os.getenv("OS_AUTH_URL")
    os.environ["OS_IDENTITY_API_VERSION"] = os.getenv("OS_IDENTITY_API_VERSION")
    os.environ["OS_USERNAME"] = os.getenv("OS_USERNAME")
    os.environ["OS_PASSWORD"] = os.getenv("OS_PASSWORD")
    os.environ["OS_TENANT_ID"] = os.getenv("OS_TENANT_ID")
    os.environ["OS_TENANT_NAME"] = os.getenv("OS_TENANT_NAME")
    os.environ["OS_REGION_NAME"] = os.getenv("OS_REGION_NAME")


def download_normalized_data():
    cwd = pathlib.Path().absolute()
    os.chdir(DATA_FOLDER)
    logging.info(f"starting to download normalized data into {DATA_FOLDER}, this could take a while")
    r = subprocess.run(["swift", "download", "--skip-identical", CONTAINER], stdout=subprocess.PIPE)
    downloaded_files = str(r.stdout)[2:-3].split("\\n")
    downloaded_files_str = '\n\t'.join(downloaded_files)
    logging.info(f"Downloaded following files: \n\t{downloaded_files_str}")


    os.chdir(cwd)
    # we return True if any file has been added or changed
    for f in downloaded_files:
        if not f.startswith("Skipped identical file"):
            return True

    # otherwise we return False
    return False


