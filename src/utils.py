import os
from typing import List

from tableschema import Schema

from src.constants import NORMALIZED_SCHEMA_DIR

POSTRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', '')


def get_all_schema(schema_dir=NORMALIZED_SCHEMA_DIR) -> List[Schema]:
    return [Schema(schema_path) for schema_path in get_all_schema_path(schema_dir)]


def get_all_schema_path(schema_dir=NORMALIZED_SCHEMA_DIR) -> List[str]:
    for root, dirs, files in os.walk(schema_dir):
        dirs.sort()
        for file in sorted(files):
            if not file.endswith('.json'):
                continue
            schema_path = os.path.join(root, file)
            yield schema_path


def is_running_in_docker():
    return True
