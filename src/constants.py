from os.path import join as pjoin

SCHEMA_DIR = "schemas"
NORMALIZED_SCHEMA_DIR = pjoin(SCHEMA_DIR, "normalized")
